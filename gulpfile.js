'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify  = require('gulp-notify');
// var header = require( 'gulp-header' );

gulp.task('sass', function(){
  gulp.src('./_src/sass/**/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(sass({outputStyle: 'expanded'}))
    // .pipe(header('@charset "UTF-8";\n'))
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', function(){
  gulp.watch('./_src/sass/**/*.scss', ['sass']);
})

gulp.task('default',['watch']);
